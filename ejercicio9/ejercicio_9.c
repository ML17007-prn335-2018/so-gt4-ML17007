#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    int palabras;
    do
    {
        printf("Ingrese la cantidad de palabras para ordenar:");
        scanf("%d", &palabras);
    } while (palabras <= 0);

    char cadenas[palabras][15];
    printf("Ingrese las palabras(No mayores de 15 letras)\n");
    for (int i = 0; i < palabras; i++)
    {
        printf("Palabra #%d:", i + 1);
        scanf("%s", cadenas[i]);
    }

    char texto[15];
    int error;

    do
    {

        error = 0;
        for (int j = 0; j < palabras - 1; j++)
        {
            if (strcmp(cadenas[j], cadenas[j + 1]) > 0)
            {
                strcpy(texto, cadenas[j]);
                strcpy(cadenas[j], cadenas[j + 1]);
                strcpy(cadenas[j + 1], texto);
                error = 1;
            }
        }

    } while (error == 1);
    printf("Palabras ordenados alfabeticamente:\n");
    for (int i = 0; i < palabras; i++)
    {
        printf("%d.%s\n", (i + 1), cadenas[i]);
    }
}