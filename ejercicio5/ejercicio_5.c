#include <stdio.h>
#include <stdlib.h>

void ordenar(int *);

int main()
{
    int *numeros = (int *)malloc(100 * sizeof(int));
    printf("Valores Random:\n");
    for (int i = 1; i <= 100; i++)
    {
        *numeros = rand() % 100;
        if (i % 10 == 0)
        {
            printf("%d\n", *numeros);
        }
        else
        {
            printf("%d--", *numeros);
        }
        numeros++;
    }
    printf("\n\n"),
        ordenar(numeros);
    return 0;
}

void ordenar(int *datos)
{
    int error = 0;
    int actual = 0;
    int anterior = 0;
    int siguiente = 0;

    do
    {
        datos--;
        error = 0;
        for (int i = 0; i < 99; i++)
        {

            actual = *datos;

            datos--;
            anterior = *datos;

            if (actual > anterior)
            {

                *datos = actual;
                datos++;
                *datos = anterior;
                error = 1;
                datos--;
            }
        }
        datos++;
        error = 0;
        for (int i = 0; i < 99; i++)
        {
            actual = *datos;
            datos++;
            siguiente = *datos;

            if (actual < siguiente)
            {
                *datos = actual;
                datos--;
                *datos = siguiente;
                error = 1;
                datos++;
            }
        }

    } while (error == 1);
    printf("Valores ordenados:\n");
    datos--;
    for (int i = 1; i <= 100; i++)
    {
        if (i % 10 == 0)
        {
            printf("%d\n", *datos);
        }
        else
        {
            printf("%d--", *datos);
        }
        datos--;
    }
}