#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
    char nombre[40];
    int dui;
    float sueldo;
} empleado;

void calculoPromedio(empleado[], int);
void ordenarPorSueldo(empleado[], int);
void ordenarPorNombre(empleado[], int);

int main()
{
    empleado emp[100];
    int tamanio;
    do
    {
        printf("Ingrese la cantidad de empleados que registrara: ");
        scanf("%d", &tamanio);
    } while (tamanio < 1);

    for (int i = 0; i < tamanio; i++)
    {
        printf("Ingrese los datos del empleado #%d\n", i + 1);
        printf("Nombre:");
        scanf("%s", &emp[i].nombre);
        printf("Dui:");
        scanf("%d", &emp[i].dui);
        printf("Sueldo:");
        scanf("%f", &emp[i].sueldo);
    }
    ordenarPorNombre(emp, tamanio);
    ordenarPorSueldo(emp, tamanio);
    calculoPromedio(emp, tamanio);
}

void calculoPromedio(empleado empleados[], int tam)
{
    float promedio = 0;
    for (int i = 0; i < tam; i++)
    {
        promedio = promedio + (empleados[i].sueldo);
    }
    promedio = promedio / tam;
    printf("El promedio de sueldo:%lf\n", promedio);
}

void ordenarPorSueldo(empleado empleados[], int tam)
{
    int error = 0;
    empleado emp;
    do
    {
        error = 0;
        for (int i = 0; i < (tam - 1); i++)
        {
            if (empleados[i].sueldo < empleados[i + 1].sueldo)
            {
                emp = empleados[i];
                empleados[i] = empleados[i + 1];
                empleados[i + 1] = emp;
                error = 1;
            }
        }
    } while (error == 1);
    printf("Empleados ordenados por sueldo(Descendente).\n");
    for (int i = 0; i < tam; i++)
    {
        printf("Empleado #%d: Nombre:%s\tDui:%d\tSueldo:$%f\n", (i + 1), empleados[i].nombre, empleados[i].dui, empleados[i].sueldo);
    }
}

void ordenarPorNombre(empleado emps[], int tam)
{
    int error = 0;
    empleado emp;
    do
    {
        error = 0;
        for (int i = 0; i < (tam - 1); i++)
        {
            if ((strcmp(emps[i].nombre, emps[i + 1].nombre)) > 0)
            {
                emp = emps[i];
                emps[i] = emps[i + 1];
                emps[i + 1] = emp;
                error = 1;
            }
        }
    } while (error == 1);
    printf("Empleados ordenados por Nombre.\n");
    for (int i = 0; i < tam; i++)
    {
        printf("Empleado #%d: Nombre:%s\tDui:%d\tSueldo:$%f\n", (i + 1), emps[i].nombre, emps[i].dui, emps[i].sueldo);
    }
}